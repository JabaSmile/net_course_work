﻿namespace WindowsFormsApp1
{
    partial class JsonMaper
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.ShowJson_tbox = new System.Windows.Forms.TextBox();
            this.SaveAsJson_btn = new System.Windows.Forms.Button();
            this.ActionsWithJson_tab = new System.Windows.Forms.TabControl();
            this.TieFields_tpage = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SelectJsonField_lb = new System.Windows.Forms.Label();
            this.TableColumns_cb = new System.Windows.Forms.ComboBox();
            this.SelecttableField_lb = new System.Windows.Forms.Label();
            this.SetField_btn = new System.Windows.Forms.Button();
            this.JsonField_cb = new System.Windows.Forms.ComboBox();
            this.AddNewField_tpage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddJsonField_tbox = new System.Windows.Forms.TextBox();
            this.AddJsonFieldName_lb = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.Select = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.ActionsWithJson_tab.SuspendLayout();
            this.TieFields_tpage.SuspendLayout();
            this.panel3.SuspendLayout();
            this.AddNewField_tpage.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.ShowJson_tbox);
            this.panel2.Controls.Add(this.SaveAsJson_btn);
            this.panel2.Controls.Add(this.ActionsWithJson_tab);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(285, 528);
            this.panel2.TabIndex = 7;
            // 
            // ShowJson_tbox
            // 
            this.ShowJson_tbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowJson_tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ShowJson_tbox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowJson_tbox.ForeColor = System.Drawing.SystemColors.Window;
            this.ShowJson_tbox.Location = new System.Drawing.Point(3, 3);
            this.ShowJson_tbox.Multiline = true;
            this.ShowJson_tbox.Name = "ShowJson_tbox";
            this.ShowJson_tbox.Size = new System.Drawing.Size(276, 281);
            this.ShowJson_tbox.TabIndex = 10;
            // 
            // SaveAsJson_btn
            // 
            this.SaveAsJson_btn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveAsJson_btn.Enabled = false;
            this.SaveAsJson_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.SaveAsJson_btn.Location = new System.Drawing.Point(20, 491);
            this.SaveAsJson_btn.Name = "SaveAsJson_btn";
            this.SaveAsJson_btn.Size = new System.Drawing.Size(242, 29);
            this.SaveAsJson_btn.TabIndex = 7;
            this.SaveAsJson_btn.Text = "Save as json";
            this.SaveAsJson_btn.UseVisualStyleBackColor = true;
            this.SaveAsJson_btn.Click += new System.EventHandler(this.SaveAsJson_btn_Click);
            // 
            // ActionsWithJson_tab
            // 
            this.ActionsWithJson_tab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ActionsWithJson_tab.Controls.Add(this.TieFields_tpage);
            this.ActionsWithJson_tab.Controls.Add(this.AddNewField_tpage);
            this.ActionsWithJson_tab.Location = new System.Drawing.Point(3, 290);
            this.ActionsWithJson_tab.Name = "ActionsWithJson_tab";
            this.ActionsWithJson_tab.SelectedIndex = 0;
            this.ActionsWithJson_tab.Size = new System.Drawing.Size(276, 193);
            this.ActionsWithJson_tab.TabIndex = 2;
            // 
            // TieFields_tpage
            // 
            this.TieFields_tpage.BackColor = System.Drawing.Color.Transparent;
            this.TieFields_tpage.Controls.Add(this.panel3);
            this.TieFields_tpage.Location = new System.Drawing.Point(4, 22);
            this.TieFields_tpage.Name = "TieFields_tpage";
            this.TieFields_tpage.Padding = new System.Windows.Forms.Padding(3);
            this.TieFields_tpage.Size = new System.Drawing.Size(268, 167);
            this.TieFields_tpage.TabIndex = 0;
            this.TieFields_tpage.Text = "Select Fields";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.SelectJsonField_lb);
            this.panel3.Controls.Add(this.TableColumns_cb);
            this.panel3.Controls.Add(this.SelecttableField_lb);
            this.panel3.Controls.Add(this.SetField_btn);
            this.panel3.Controls.Add(this.JsonField_cb);
            this.panel3.Location = new System.Drawing.Point(7, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(255, 146);
            this.panel3.TabIndex = 2;
            // 
            // SelectJsonField_lb
            // 
            this.SelectJsonField_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectJsonField_lb.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectJsonField_lb.Location = new System.Drawing.Point(6, 6);
            this.SelectJsonField_lb.Name = "SelectJsonField_lb";
            this.SelectJsonField_lb.Size = new System.Drawing.Size(244, 20);
            this.SelectJsonField_lb.TabIndex = 9;
            this.SelectJsonField_lb.Text = "Select json field";
            this.SelectJsonField_lb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TableColumns_cb
            // 
            this.TableColumns_cb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TableColumns_cb.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableColumns_cb.FormattingEnabled = true;
            this.TableColumns_cb.Location = new System.Drawing.Point(6, 80);
            this.TableColumns_cb.Name = "TableColumns_cb";
            this.TableColumns_cb.Size = new System.Drawing.Size(242, 25);
            this.TableColumns_cb.TabIndex = 8;
            // 
            // SelecttableField_lb
            // 
            this.SelecttableField_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelecttableField_lb.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelecttableField_lb.Location = new System.Drawing.Point(6, 57);
            this.SelecttableField_lb.Name = "SelecttableField_lb";
            this.SelecttableField_lb.Size = new System.Drawing.Size(244, 20);
            this.SelecttableField_lb.TabIndex = 9;
            this.SelecttableField_lb.Text = "Select table field";
            this.SelecttableField_lb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetField_btn
            // 
            this.SetField_btn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SetField_btn.Enabled = false;
            this.SetField_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.SetField_btn.Location = new System.Drawing.Point(6, 111);
            this.SetField_btn.Name = "SetField_btn";
            this.SetField_btn.Size = new System.Drawing.Size(244, 29);
            this.SetField_btn.TabIndex = 7;
            this.SetField_btn.Text = "Set Field";
            this.SetField_btn.UseVisualStyleBackColor = true;
            this.SetField_btn.Click += new System.EventHandler(this.SetField_btn_Click);
            // 
            // JsonField_cb
            // 
            this.JsonField_cb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.JsonField_cb.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JsonField_cb.FormattingEnabled = true;
            this.JsonField_cb.Location = new System.Drawing.Point(6, 29);
            this.JsonField_cb.Name = "JsonField_cb";
            this.JsonField_cb.Size = new System.Drawing.Size(242, 25);
            this.JsonField_cb.TabIndex = 8;
            // 
            // AddNewField_tpage
            // 
            this.AddNewField_tpage.Controls.Add(this.panel1);
            this.AddNewField_tpage.Location = new System.Drawing.Point(4, 22);
            this.AddNewField_tpage.Name = "AddNewField_tpage";
            this.AddNewField_tpage.Padding = new System.Windows.Forms.Padding(3);
            this.AddNewField_tpage.Size = new System.Drawing.Size(268, 167);
            this.AddNewField_tpage.TabIndex = 1;
            this.AddNewField_tpage.Text = "tabPage2";
            this.AddNewField_tpage.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.AddJsonField_tbox);
            this.panel1.Controls.Add(this.AddJsonFieldName_lb);
            this.panel1.Controls.Add(this.comboBox3);
            this.panel1.Controls.Add(this.Select);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(7, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(255, 146);
            this.panel1.TabIndex = 2;
            // 
            // AddJsonField_tbox
            // 
            this.AddJsonField_tbox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.AddJsonField_tbox.Location = new System.Drawing.Point(6, 34);
            this.AddJsonField_tbox.Name = "AddJsonField_tbox";
            this.AddJsonField_tbox.Size = new System.Drawing.Size(242, 23);
            this.AddJsonField_tbox.TabIndex = 10;
            // 
            // AddJsonFieldName_lb
            // 
            this.AddJsonFieldName_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddJsonFieldName_lb.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddJsonFieldName_lb.Location = new System.Drawing.Point(6, 6);
            this.AddJsonFieldName_lb.Name = "AddJsonFieldName_lb";
            this.AddJsonFieldName_lb.Size = new System.Drawing.Size(244, 20);
            this.AddJsonFieldName_lb.TabIndex = 9;
            this.AddJsonFieldName_lb.Text = "Select json field";
            this.AddJsonFieldName_lb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox3
            // 
            this.comboBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 80);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(242, 25);
            this.comboBox3.TabIndex = 8;
            // 
            // Select
            // 
            this.Select.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Select.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Select.Location = new System.Drawing.Point(6, 57);
            this.Select.Name = "Select";
            this.Select.Size = new System.Drawing.Size(244, 20);
            this.Select.TabIndex = 9;
            this.Select.Text = "Select table field";
            this.Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button2.Location = new System.Drawing.Point(6, 111);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(244, 29);
            this.button2.TabIndex = 7;
            this.button2.Text = "Set Field";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // JsonMaper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel2);
            this.Name = "JsonMaper";
            this.Size = new System.Drawing.Size(291, 534);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ActionsWithJson_tab.ResumeLayout(false);
            this.TieFields_tpage.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.AddNewField_tpage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox ShowJson_tbox;
        private System.Windows.Forms.TabControl ActionsWithJson_tab;
        private System.Windows.Forms.TabPage TieFields_tpage;
        private System.Windows.Forms.TabPage AddNewField_tpage;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label SelectJsonField_lb;
        private System.Windows.Forms.ComboBox TableColumns_cb;
        private System.Windows.Forms.Label SelecttableField_lb;
        private System.Windows.Forms.Button SetField_btn;
        private System.Windows.Forms.ComboBox JsonField_cb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label AddJsonFieldName_lb;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label Select;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button SaveAsJson_btn;
        private System.Windows.Forms.TextBox AddJsonField_tbox;
    }
}
