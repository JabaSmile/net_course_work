﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using System.Data.OleDb;
using SystemDataTable = System.Data.DataTable;

namespace WindowsFormsApp1
{
    public partial class LoadTable : UserControl
    {
        public LoadTable()
        {
            InitializeComponent();

            this.ShowTable_dgv.ReadOnly = false;
            this.ShowTable_dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        public MyFile myFile { get; set; }

        public ExcelConverter excelConverter { get; set; } = new ExcelConverter();
        public CsvConverter csvConverter { get; set; } = new CsvConverter();
        public XmlConverter xmlConverter { get; set; } = new XmlConverter();


        public DataGridView dgv { get; set; }

        public delegate void Action();
        public event Action TableLoaded;



        public void EnableloadBtn()
        {
            this.Load_btn.Enabled = true;
        }

        public SystemDataTable LoadFile()
        {
            try
            {
                if (myFile.fExtension == ".xlsx" || myFile.fExtension == ".xls")
                {
                    return LoadExelFile();
                }
                else if (myFile.fExtension == ".csv" || myFile.fExtension == ".txt")
                {
                    return LoadCsvFile();
                }
                else if (myFile.fExtension == ".xml")
                {
                    return LoadXmlFile();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, exc.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return null;
        }

        private SystemDataTable LoadXmlFile()
        {
            return xmlConverter.GetXml(myFile.fPath);
        }

        public SystemDataTable LoadExelFile()
        {          
            using (var con = new OleDbConnection(excelConverter.GetFileFormatConnectioString(myFile.fExtension, myFile.fPath)))
            {
                con.Open();

                this.Info_cb.Items.AddRange(excelConverter.GetSheets(con).ToArray());

                if (this.Info_cb.Text == string.Empty)
                {
                    this.Info_cb.Text = excelConverter.GetSheets(con).First();
                }

                //Set data to DataGridView
                return new ExcelConverter().GetDataTable(con, new SystemDataTable(), this.Info_cb.Text);
            }
        }

        private SystemDataTable LoadCsvFile()
        {
            this.Info_cb.Items.AddRange(csvConverter.GetDelimiters().ToArray());

            if(this.Info_cb.Text == string.Empty)
            {
                this.Info_cb.Text = csvConverter.GetDelimiters().First();
            }

            return new CsvConverter().ReadCsv(myFile.fPath, this.Info_cb.Text);
        }


        private void Load_btn_Click(object sender, EventArgs e)
        {
            this.Info_cb.Items.Clear();

            this.ShowTable_dgv.DataSource = LoadFile();
            dgv = this.ShowTable_dgv;

            TableLoaded?.Invoke();
        }
    }
}
