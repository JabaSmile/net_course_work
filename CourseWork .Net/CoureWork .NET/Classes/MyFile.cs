﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class MyFile
    {
        public string fName { get; set; }

        public long fSie { get; set; }

        public string fPath { get; set; }

        public string fExtension { get; set; }
    }
}
