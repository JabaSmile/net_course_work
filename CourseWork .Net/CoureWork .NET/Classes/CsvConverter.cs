﻿using CsvHelper;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SystemDataTable = System.Data.DataTable;

namespace WindowsFormsApp1
{
    public class CsvConverter : IConverter
    {       
        //ONLY FOR CSV: read csv file
        public SystemDataTable readCSV(string filePath, string delimetr)
        {
            var dt = new SystemDataTable();

            // Creating the columns
            foreach (var headerLine in File.ReadLines(filePath).Take(1))
            {
                foreach (var headerItem in headerLine.Split(new[] { Convert.ToChar(delimetr) }, StringSplitOptions.RemoveEmptyEntries))
                {
                    dt.Columns.Add(headerItem.Trim());
                }
            }

            // Adding the rows
            foreach (var line in File.ReadLines(filePath).Skip(1))
            {
                dt.Rows.Add(line.Split(Convert.ToChar(delimetr)));
            }

            return dt;
        }

        public List<string> GetFieldsAsList(string absolutePath)
        {
            List<string> result = new List<string>();
            string value;

            using (TextReader fileReader = File.OpenText(absolutePath))
            {
                using (var csv = new CsvReader(fileReader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;

                    while (csv.Read())
                    {
                        for (int i = 0; csv.TryGetField<string>(i, out value); i++)
                        {
                            result.Add(value);
                        }
                    }
                }                   
            }

            return result;
        }

        public SystemDataTable ReadCsv(string fPath, string delimetr = ",")
        {
            using(var reader = new StreamReader(fPath))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture)) 
                {
                    csv.Configuration.Delimiter = delimetr;

                    //csv.Configuration.RegisterClassMap<FooMap>();
                    List<IDictionary<string, object>> csvData = new List<IDictionary<string, object>>();
                    IDictionary<string, object> csvHeader;

                    //Get Header
                    csv.Read();
                    csv.ReadHeader();
                    csvHeader = csv.GetRecord<dynamic>() as IDictionary<string, object>;

                    while (csv.Read())
                    {
                        csvData.Add(csv.GetRecord<dynamic>() as IDictionary<string, object>);
                    }

                    var dt = new SystemDataTable();

                    foreach (var header in csvHeader)
                    {
                        dt.Columns.Add(header.Value.ToString());
                    }

                    foreach (var line in csvData)
                    {
                        dt.Rows.Add(line.Values.ToArray());
                    }

                    return dt;
                }
            }
        }

        //Set delimetrs to ComboBox
        public List<string> GetDelimiters()
        {
            return new List<string>()
            {
                ";",",",":","\'","\"","\\","/"
            };
        }

        public SystemDataTable LoadData(MyFile myFile)
        {
            //return new CsvConverter().readCSV(myFile.fPath, this.Info_cb.Text);

            return new CsvConverter().ReadCsv(myFile.fPath, GetDelimiters().First());
        }
    }
}
