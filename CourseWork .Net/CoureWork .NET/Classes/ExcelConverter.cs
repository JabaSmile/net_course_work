﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SystemDataTable = System.Data.DataTable;

namespace WindowsFormsApp1
{
    public class ExcelConverter : IConverter
    {
        //Return filled DataTable 
        public SystemDataTable GetDataTable(OleDbConnection con, SystemDataTable dt, string qwery)
        {
            //Create OleCommand
            var cmd = new OleDbCommand($"SELECT * FROM [" + qwery + "$]", con);

            //Fill data table of Excel data
            var da = new OleDbDataAdapter(cmd);

            //var dt = new SystemDataTable();
            da.Fill(dt);

           

            return dt;
        }

        //ONLY ECXEL FORMAT: get sheets of excel file
        public List<string> GetSheets(OleDbConnection con)
        {
            //Get DataTable
            SystemDataTable dtSheet = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            var tmpList = new List<string>();

            //Get Sheets name & write name to listSheet
            foreach (DataRow drSheet in dtSheet.Rows)
            {
                if (drSheet["TABLE_NAME"].ToString().Contains("$"))//checks whether row contains '_xlnm#_FilterDatabase' or sheet name(i.e. sheet name always ends with $ sign)
                {
                    var shName = (drSheet["TABLE_NAME"].ToString().Replace("$", "").Replace("\'", ""));
                    tmpList.Add(shName);
                }
            }

            return tmpList;
        }

        //convert  to valid connection string 
        public string GetFileFormatConnectioString(string fExctencion, string fPath)
        {
            var connectionString = new Dictionary<string, string>()
            {
                {
                    ".xls",
                    $"Provider=Microsoft.Jet.OLEDB.4.0;" +
                    $"Data Source={fPath};" +
                    $"Extended Properties='Excel 8.0; HDR=Yes; IMEX=1;'"
                },
                {
                    ".xlsx",
                    $"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    $"Data Source={fPath};" +
                    $"Extended Properties='Excel 12.0; HDR=Yes; IMEX=1;'"
                }
            };

            foreach (var item in connectionString)
            {
                if (fExctencion == item.Key)
                {
                    return item.Value;
                }
            }

            return null;
        }

        public SystemDataTable LoadData(MyFile myFile)
        {
            using (var con = new OleDbConnection(GetFileFormatConnectioString(myFile.fExtension, myFile.fPath)))
            {
                con.Open();

                //Set data to DataGridView
                return new ExcelConverter().GetDataTable(con, new SystemDataTable(), GetSheets(con).First());
            }
        }

        //ONLY ECXEL FORMAT: set default sheets
        public string setDefaultSheetName(List<string> choiceList)
        {
            //Check TextBox to empty string 
            //Set default name

            string constSheerName = string.Empty;
            if (choiceList[0] != null)
            {
                constSheerName = choiceList[0].ToString();
            }

            return constSheerName;
        }
    }
}
