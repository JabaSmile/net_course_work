﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemDataTable = System.Data.DataTable;

namespace WindowsFormsApp1
{
    public interface IConverter
    {
        SystemDataTable LoadData(MyFile myFile);
    }
}
