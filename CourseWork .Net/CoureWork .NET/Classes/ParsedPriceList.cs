﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class ParsedPriceList
    {
        //ToDO: Add json fields
        public string Id { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }

        public string InStock { get; set; }

        //public List<Dictionary<string, string>> CustomFields { get; set; }
    }
}
