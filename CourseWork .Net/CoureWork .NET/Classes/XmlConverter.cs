﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SystemDataTable = System.Data.DataTable;

namespace WindowsFormsApp1
{
    public class XmlConverter : IConverter
    {
        //ONLY FOR XML: read xml file
        public DataTable GetXml(string fPath)
        {
            DataSet ds = new DataSet("Sample");
            ds.ReadXml(fPath);
           
            return ds.Tables[0];
        }

        public SystemDataTable LoadData(MyFile myFile)
        {
            throw new NotImplementedException();
        }
    }
}
