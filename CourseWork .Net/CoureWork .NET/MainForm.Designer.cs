﻿namespace WindowsFormsApp1
{
    partial class UniversalPriceHandler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            WindowsFormsApp1.MyFile myFile1 = new WindowsFormsApp1.MyFile();
            WindowsFormsApp1.CsvConverter csvConverter1 = new WindowsFormsApp1.CsvConverter();
            WindowsFormsApp1.ExcelConverter excelConverter1 = new WindowsFormsApp1.ExcelConverter();
            WindowsFormsApp1.XmlConverter xmlConverter1 = new WindowsFormsApp1.XmlConverter();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.jsonMaper1 = new WindowsFormsApp1.JsonMaper();
            this.loadTable1 = new WindowsFormsApp1.LoadTable();
            this.loadFile1 = new WindowsFormsApp1.LoadFile();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.jsonMaper1);
            this.MainPanel.Controls.Add(this.loadTable1);
            this.MainPanel.Controls.Add(this.loadFile1);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(940, 579);
            this.MainPanel.TabIndex = 0;
            // 
            // jsonMaper1
            // 
            this.jsonMaper1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jsonMaper1.BackColor = System.Drawing.SystemColors.Control;
            this.jsonMaper1.dgv = null;
            this.jsonMaper1.Location = new System.Drawing.Point(661, 12);
            myFile1.fExtension = null;
            myFile1.fName = null;
            myFile1.fPath = null;
            myFile1.fSie = ((long)(0));
            this.jsonMaper1.myFile = myFile1;
            this.jsonMaper1.Name = "jsonMaper1";
            this.jsonMaper1.Size = new System.Drawing.Size(267, 555);
            this.jsonMaper1.TabIndex = 13;
            // 
            // loadTable1
            // 
            this.loadTable1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loadTable1.csvConverter = csvConverter1;
            this.loadTable1.dgv = null;
            this.loadTable1.excelConverter = excelConverter1;
            this.loadTable1.Location = new System.Drawing.Point(12, 178);
            this.loadTable1.myFile = null;
            this.loadTable1.Name = "loadTable1";
            this.loadTable1.Size = new System.Drawing.Size(652, 389);
            this.loadTable1.TabIndex = 2;
            this.loadTable1.xmlConverter = xmlConverter1;
            // 
            // loadFile1
            // 
            this.loadFile1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loadFile1.Location = new System.Drawing.Point(12, 6);
            this.loadFile1.myFile = null;
            this.loadFile1.Name = "loadFile1";
            this.loadFile1.Size = new System.Drawing.Size(643, 165);
            this.loadFile1.TabIndex = 0;
            this.loadFile1.Load += new System.EventHandler(this.loadFile1_Load);
            // 
            // UniversalPriceHandler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 579);
            this.Controls.Add(this.MainPanel);
            this.MinimumSize = new System.Drawing.Size(601, 455);
            this.Name = "UniversalPriceHandler";
            this.Text = "Universal price handler";
            this.MainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        private LoadFile loadFile1;
        private LoadTable loadTable1;
        private JsonMaper jsonMaper1;
    }
}

