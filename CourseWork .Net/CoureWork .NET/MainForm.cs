﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using SystemDataTable = System.Data.DataTable;
using System.Threading;
using SpXls = Spire.Xls;
using MIF = Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Xml;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.Globalization;
using CsvHelper;

namespace WindowsFormsApp1
{

    public partial class UniversalPriceHandler : Form
    {
        
        public UniversalPriceHandler()
        {
            InitializeComponent();
            

            AllowDrop = true;

            Debug.WriteLine($"Form {this.Name} open in Thread #{Thread.CurrentThread.ManagedThreadId}");

            //Set enable true Load btn
            loadFile1.FileLoaded += loadTable1.EnableloadBtn;

            //Set myFile to user controls
            loadFile1.FileLoaded += delegate
            {
                loadTable1.myFile = loadFile1.myFile;
                jsonMaper1.myFile = loadFile1.myFile;
            };

            //Set enable true Save btn
            loadFile1.FileLoaded += jsonMaper1.EnableSaveBtn;

            //Set DataGridView to user controls
            loadTable1.TableLoaded += delegate
            {
                jsonMaper1.dgv = loadTable1.dgv;
            };

            //Inicialize settings json file
            loadTable1.TableLoaded += jsonMaper1.Inicialise;

            //Set table fields to ComboBox
            loadTable1.TableLoaded += delegate
            {
                var list = new List<string>();
                foreach (DataGridViewColumn t in loadTable1.dgv.Columns)
                {
                    list.Add(t.DataPropertyName);
                }

                jsonMaper1.SetTableColumnsToComboBox(list);
            };


        }

        //inic classes
        public static MyFile myFile = new MyFile();
        public static XmlConverter converter = new XmlConverter();

        //variable to save musnt data
        public static DataGridView dgv;
        public static List<string> choiceList= new List<string>();

        private void loadFile1_Load(object sender, EventArgs e)
        {

        }
    }    
}


