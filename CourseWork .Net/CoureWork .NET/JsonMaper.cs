﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Spire.Pdf.Exporting.XPS.Schema;
using System.Reflection;
using System.Linq.Expressions;
using Newtonsoft.Json.Serialization;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class JsonMaper : UserControl
    {
        public JsonMaper()
        {
            InitializeComponent();

            string json = JsonConvert.SerializeObject(new ParsedPriceList(), Formatting.Indented);
            this.ShowJson_tbox.Text = json;
            this.ShowJson_tbox.ReadOnly = true;


            this.JsonField_cb.Items.AddRange(GetJsonColumns().ToArray());
            this.JsonField_cb.Text = GetJsonColumns().First();
        }

        public MyFile myFile { get; set; } = new MyFile();
        public DataGridView dgv { get; set; }

        public static List<ParsedPriceList> parsedLists = new List<ParsedPriceList>();

        public static Dictionary<string, Action<ParsedPriceList, object>> setters = GetSetters();


        public void SetTableColumnsToComboBox(List<string> list)
        {
            if(this.TableColumns_cb.Items.Count > 0)
            {
                this.TableColumns_cb.Items.Clear();
            }

            this.TableColumns_cb.Items.AddRange(list.ToArray());
            this.TableColumns_cb.Text = list.First();
        }

        public List<string> GetJsonColumns()
        {
            ParsedPriceList jsonPrices = new ParsedPriceList();
            List<string> fieldsNameList = new List<string>();

            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(jsonPrices))
            {
                fieldsNameList.Add(descriptor.Name);
            }

            return fieldsNameList;
        }
     
        public void Inicialise()
        {
            if (parsedLists.Count > 0)
            {
                parsedLists.Clear();
            }

            for (int row = 0; row < dgv.Rows.Count; row++) 
            {
                

                parsedLists.Add(new ParsedPriceList());
            }

            this.SetField_btn.Enabled = true;
        }     

        private void SetField_btn_Click(object sender, EventArgs e)
        {
            var jsonSelectedField = this.JsonField_cb.Text;
            var tableSelectedField = this.TableColumns_cb.Text;

            for (var row = 0; row < dgv.Rows.Count; row++)
            {
                var columnValue = (string)dgv.Rows[row].Cells[tableSelectedField].Value;

                var setter = setters[jsonSelectedField];

                setter?.Invoke(parsedLists[row], columnValue);

            }

            this.ShowJson_tbox.Text = JsonConvert.SerializeObject(parsedLists.FirstOrDefault(), Formatting.Indented);
        }

        public static Action<TObj, object> GetSetter<TObj>(string propertyName)
        {
            var propInfo = typeof(TObj).GetProperty(propertyName);
            var propType = propInfo.PropertyType;
            var setMethod = propInfo.GetSetMethod();

            var objParam = Expression.Parameter(typeof(TObj), "object");
            var setValuePapam = Expression.Parameter(typeof(object), "setValue");

            var convertExpression = Expression.Convert(setValuePapam, propType);
            var callExpression = Expression.Call(objParam, setMethod, convertExpression);

            var lambda =
                Expression.Lambda<Action<TObj, object>>(callExpression, objParam, setValuePapam);

            return lambda.Compile();
        }

        private static Dictionary<string, Action<ParsedPriceList, object>> GetSetters()
        {
            var jsonRes = new ParsedPriceList();
            var jsonResType = jsonRes.GetType();

            var propNamesSetterDict = TypeDescriptor.GetProperties(jsonRes)
                .Cast<PropertyDescriptor>()
                .ToDictionary(
                pd => pd.Name,
                pd => GetSetter<ParsedPriceList>(pd.Name));

            return propNamesSetterDict;
        }

        public void EnableSaveBtn()
        {
            this.SaveAsJson_btn.Enabled = true;
        }

        private void SaveAsJson_btn_Click(object sender, EventArgs e)
        {
            try
            {
                for (var i = 0; true; i++) 
                {
                    var currentDirectory = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).ToString()).ToString();

                    if(myFile.fName != null)
                    {
                        if(!Directory.Exists($@"{currentDirectory}\Saves"))
                        {
                            Directory.CreateDirectory($@"{currentDirectory}\Saves");
                        }

                        if (!File.Exists($@"{currentDirectory}\Saves\{myFile.fName}({i}).json"))
                        {
                            File.WriteAllText($@"{currentDirectory}\Saves\{myFile.fName}({i}).json", JsonConvert.SerializeObject(parsedLists));

                            MessageBox.Show($@"File was saved to {currentDirectory}\Saves as {myFile.fName}({i})", "Saved Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }                   
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, exc.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
