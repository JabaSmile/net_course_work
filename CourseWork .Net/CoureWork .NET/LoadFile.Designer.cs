﻿namespace WindowsFormsApp1
{
    partial class LoadFile
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infopanel = new System.Windows.Forms.Panel();
            this.FileExtension_lb = new System.Windows.Forms.Label();
            this.FileSize_lb = new System.Windows.Forms.Label();
            this.FilePath_tbox = new System.Windows.Forms.TextBox();
            this.FileExtensionValue_lb = new System.Windows.Forms.Label();
            this.FileSizevalue_lb = new System.Windows.Forms.Label();
            this.FileNameValue_lb = new System.Windows.Forms.Label();
            this.FilePathValue_lb = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FileName_lb = new System.Windows.Forms.Label();
            this.FilePath_lb = new System.Windows.Forms.Label();
            this.ChoiceFile_btn = new System.Windows.Forms.Button();
            this.infopanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // infopanel
            // 
            this.infopanel.Controls.Add(this.FileExtension_lb);
            this.infopanel.Controls.Add(this.FileSize_lb);
            this.infopanel.Controls.Add(this.FilePath_tbox);
            this.infopanel.Controls.Add(this.FileExtensionValue_lb);
            this.infopanel.Controls.Add(this.FileSizevalue_lb);
            this.infopanel.Controls.Add(this.FileNameValue_lb);
            this.infopanel.Controls.Add(this.FilePathValue_lb);
            this.infopanel.Controls.Add(this.label1);
            this.infopanel.Controls.Add(this.FileName_lb);
            this.infopanel.Controls.Add(this.FilePath_lb);
            this.infopanel.Controls.Add(this.ChoiceFile_btn);
            this.infopanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infopanel.Location = new System.Drawing.Point(0, 0);
            this.infopanel.Name = "infopanel";
            this.infopanel.Size = new System.Drawing.Size(445, 165);
            this.infopanel.TabIndex = 3;
            // 
            // FileExtension_lb
            // 
            this.FileExtension_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileExtension_lb.Location = new System.Drawing.Point(5, 137);
            this.FileExtension_lb.Name = "FileExtension_lb";
            this.FileExtension_lb.Size = new System.Drawing.Size(112, 23);
            this.FileExtension_lb.TabIndex = 2;
            this.FileExtension_lb.Text = "File Extension:";
            this.FileExtension_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FileSize_lb
            // 
            this.FileSize_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileSize_lb.Location = new System.Drawing.Point(5, 114);
            this.FileSize_lb.Name = "FileSize_lb";
            this.FileSize_lb.Size = new System.Drawing.Size(85, 23);
            this.FileSize_lb.TabIndex = 2;
            this.FileSize_lb.Text = "File Size:";
            this.FileSize_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FilePath_tbox
            // 
            this.FilePath_tbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FilePath_tbox.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePath_tbox.Location = new System.Drawing.Point(5, 8);
            this.FilePath_tbox.Multiline = true;
            this.FilePath_tbox.Name = "FilePath_tbox";
            this.FilePath_tbox.Size = new System.Drawing.Size(356, 27);
            this.FilePath_tbox.TabIndex = 0;
            // 
            // FileExtensionValue_lb
            // 
            this.FileExtensionValue_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileExtensionValue_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileExtensionValue_lb.Location = new System.Drawing.Point(123, 137);
            this.FileExtensionValue_lb.Name = "FileExtensionValue_lb";
            this.FileExtensionValue_lb.Size = new System.Drawing.Size(303, 23);
            this.FileExtensionValue_lb.TabIndex = 2;
            this.FileExtensionValue_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FileSizevalue_lb
            // 
            this.FileSizevalue_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileSizevalue_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileSizevalue_lb.Location = new System.Drawing.Point(122, 114);
            this.FileSizevalue_lb.Name = "FileSizevalue_lb";
            this.FileSizevalue_lb.Size = new System.Drawing.Size(303, 23);
            this.FileSizevalue_lb.TabIndex = 2;
            this.FileSizevalue_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FileNameValue_lb
            // 
            this.FileNameValue_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileNameValue_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileNameValue_lb.Location = new System.Drawing.Point(122, 68);
            this.FileNameValue_lb.Name = "FileNameValue_lb";
            this.FileNameValue_lb.Size = new System.Drawing.Size(303, 23);
            this.FileNameValue_lb.TabIndex = 2;
            // 
            // FilePathValue_lb
            // 
            this.FilePathValue_lb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FilePathValue_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePathValue_lb.Location = new System.Drawing.Point(122, 91);
            this.FilePathValue_lb.Name = "FilePathValue_lb";
            this.FilePathValue_lb.Size = new System.Drawing.Size(303, 23);
            this.FilePathValue_lb.TabIndex = 2;
            this.FilePathValue_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Version:";
            // 
            // FileName_lb
            // 
            this.FileName_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileName_lb.Location = new System.Drawing.Point(5, 68);
            this.FileName_lb.Name = "FileName_lb";
            this.FileName_lb.Size = new System.Drawing.Size(85, 23);
            this.FileName_lb.TabIndex = 2;
            this.FileName_lb.Text = "File Name:";
            // 
            // FilePath_lb
            // 
            this.FilePath_lb.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePath_lb.Location = new System.Drawing.Point(5, 91);
            this.FilePath_lb.Name = "FilePath_lb";
            this.FilePath_lb.Size = new System.Drawing.Size(85, 23);
            this.FilePath_lb.TabIndex = 2;
            this.FilePath_lb.Text = "File Path:";
            this.FilePath_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ChoiceFile_btn
            // 
            this.ChoiceFile_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChoiceFile_btn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChoiceFile_btn.Location = new System.Drawing.Point(367, 9);
            this.ChoiceFile_btn.Name = "ChoiceFile_btn";
            this.ChoiceFile_btn.Size = new System.Drawing.Size(75, 27);
            this.ChoiceFile_btn.TabIndex = 1;
            this.ChoiceFile_btn.Text = "Search";
            this.ChoiceFile_btn.UseVisualStyleBackColor = true;
            this.ChoiceFile_btn.Click += new System.EventHandler(this.ChoiceFile_btn_Click);
            // 
            // LoadFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infopanel);
            this.Name = "LoadFile";
            this.Size = new System.Drawing.Size(445, 165);
            this.infopanel.ResumeLayout(false);
            this.infopanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel infopanel;
        private System.Windows.Forms.Label FileExtension_lb;
        private System.Windows.Forms.Label FileSize_lb;
        private System.Windows.Forms.TextBox FilePath_tbox;
        private System.Windows.Forms.Label FileExtensionValue_lb;
        private System.Windows.Forms.Label FileSizevalue_lb;
        private System.Windows.Forms.Label FileNameValue_lb;
        private System.Windows.Forms.Label FilePathValue_lb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label FileName_lb;
        private System.Windows.Forms.Label FilePath_lb;
        private System.Windows.Forms.Button ChoiceFile_btn;
    }
}
