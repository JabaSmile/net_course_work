﻿namespace WindowsFormsApp1
{
    partial class LoadTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new System.Windows.Forms.Panel();
            this.ShowTable_dgv = new System.Windows.Forms.DataGridView();
            this.Info_cb = new System.Windows.Forms.ComboBox();
            this.Load_btn = new System.Windows.Forms.Button();
            this.Qwery_txb = new System.Windows.Forms.TextBox();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShowTable_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.ShowTable_dgv);
            this.MainPanel.Controls.Add(this.Info_cb);
            this.MainPanel.Controls.Add(this.Load_btn);
            this.MainPanel.Controls.Add(this.Qwery_txb);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(652, 323);
            this.MainPanel.TabIndex = 4;
            // 
            // ShowTable_dgv
            // 
            this.ShowTable_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowTable_dgv.Location = new System.Drawing.Point(5, 38);
            this.ShowTable_dgv.Name = "ShowTable_dgv";
            this.ShowTable_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ShowTable_dgv.Size = new System.Drawing.Size(641, 282);
            this.ShowTable_dgv.TabIndex = 2;
            // 
            // Info_cb
            // 
            this.Info_cb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Info_cb.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.Info_cb.FormattingEnabled = true;
            this.Info_cb.Location = new System.Drawing.Point(402, 5);
            this.Info_cb.Name = "Info_cb";
            this.Info_cb.Size = new System.Drawing.Size(163, 28);
            this.Info_cb.TabIndex = 0;
            // 
            // Load_btn
            // 
            this.Load_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Load_btn.Enabled = false;
            this.Load_btn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Load_btn.Location = new System.Drawing.Point(571, 5);
            this.Load_btn.Name = "Load_btn";
            this.Load_btn.Size = new System.Drawing.Size(75, 28);
            this.Load_btn.TabIndex = 1;
            this.Load_btn.Text = "Load";
            this.Load_btn.UseVisualStyleBackColor = true;
            this.Load_btn.Click += new System.EventHandler(this.Load_btn_Click);
            // 
            // Qwery_txb
            // 
            this.Qwery_txb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Qwery_txb.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.Qwery_txb.Location = new System.Drawing.Point(5, 5);
            this.Qwery_txb.Multiline = true;
            this.Qwery_txb.Name = "Qwery_txb";
            this.Qwery_txb.Size = new System.Drawing.Size(391, 28);
            this.Qwery_txb.TabIndex = 0;
            // 
            // LoadTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainPanel);
            this.Name = "LoadTable";
            this.Size = new System.Drawing.Size(652, 323);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShowTable_dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.DataGridView ShowTable_dgv;
        private System.Windows.Forms.ComboBox Info_cb;
        private System.Windows.Forms.Button Load_btn;
        private System.Windows.Forms.TextBox Qwery_txb;
    }
}
