﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CsvHelper.Configuration.Attributes;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class LoadFile : UserControl
    {
        public LoadFile()
        {
            InitializeComponent();
        }

        public MyFile myFile { get; set; }       

        public event Action FileLoaded;

        private static List<string> SupportedTypes { get; set; } = new List<string>()
        {
            ".xlsx", ".xls", ".csv",".txt",".xml"
        };



        public void ChoiceFile_btn_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new OpenFileDialog())
            {
                fileDialog.InitialDirectory = @"C:\Desktop";
                fileDialog.Filter = @"all files         (*.*)          |*.*;
                                     |txt  & csv files  (*.txt,.csv)   |*.txt; *csv;
                                     |xlsx & xls files  (*.xlsx,.xls)  |*.xlsx; *.xls;";


                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    FileInfo fInfo = new FileInfo(fileDialog.FileName);

                    if (IsSupportedType(SupportedTypes, fInfo.Extension)) 
                    {
                        try
                        {
                            SetFileInfoInUI(fInfo);
                            myFile = SetFileInfo(fInfo);

                            FileLoaded?.Invoke();
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.Message, exc.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("This file unsupport now, " +
                            "\ntry to update application or write to our mail: this_Is_Our_Mail@app.com",
                            "Unsupported Type", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        //Chech file to supported extensions
        public bool IsSupportedType(List<string> types, string ourType)
        {
            foreach(var item in types)
            {
                if(item == ourType)
                {
                    return true;
                }
            }

            return false;
        }

        //Set file info in UI
        private void SetFileInfoInUI(FileInfo fileInfo)
        {
            SetFileInfo(fileInfo);

            //set UI data
            this.FilePathValue_lb.Text = fileInfo.FullName;
            this.FilePath_tbox.Text = fileInfo.FullName;

            this.FileSizevalue_lb.Text = fileInfo.Length.ToString() + " bytes";

            this.FileExtensionValue_lb.Text = fileInfo.Extension;

            var fileName = fileInfo.Name;
            this.FileNameValue_lb.Text = fileName.Replace(fileInfo.Extension, "");
        }

        //Set data to MyFile class
        private MyFile SetFileInfo(FileInfo fileInfo)
        {
            MyFile myFile = new MyFile();

            //set fields if MyFile class
            myFile.fExtension = fileInfo.Extension;
            myFile.fName = fileInfo.Name.Replace(fileInfo.Extension, "");
            myFile.fSie = fileInfo.Length;
            myFile.fPath = fileInfo.FullName;

            return myFile;
        }
    }
}
